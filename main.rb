t = gets.to_i

digits = {
  0 => 'ZERO',
  2 => 'TWO',
  4 => 'FOUR',
  6 => 'SIX',
  8 => 'EIGHT',

  7 => 'SEVEN',
  5 => 'FIVE',
  3 => 'THREE',
  9 => 'NINE',
  1 => 'ONE',
}

1.upto(t) do |i|
  given_string = gets.delete("\n")
  nums = []

  digits.each do |k,v|
    temp = given_string.clone
    # puts "temp = #{temp}"
    while v.chars.all? { |char| temp.sub!(char, '') }
      given_string = temp.clone
      nums << k
    end
  end

  puts "Case ##{i}: #{nums.sort.join}"
end
